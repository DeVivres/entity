﻿using AutoMapper;
using Project.Common.DTO;
using Project.DAL.Entities;

namespace Project.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>().ReverseMap();
        }
    }
}
