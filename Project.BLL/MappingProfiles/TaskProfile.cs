﻿using AutoMapper;
using Project.Common.DTO;
using Project.DAL.Entities;

namespace Project.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>().ReverseMap();
        }
    }
}
