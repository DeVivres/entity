﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.BLL.Services
{
    public class TeamsService : IService<TeamDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(TeamDTO item)
        {
            var mapped = _mapper.Map<Team>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            _unitOfWork.Teams.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var result = _unitOfWork.Teams.Delete(id);
            _unitOfWork.SaveChanges();
            return result;
        }

        public TeamDTO Get(int id)
        {
            var result = _unitOfWork.Teams.Get(id);
            return _mapper.Map<TeamDTO>(result);
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            var result = _unitOfWork.Teams.GetAll();
            return _mapper.Map<IEnumerable<TeamDTO>>(result);
        }

        public bool Update(TeamDTO item)
        {
            var status = false;
            var mapped = _mapper.Map<Team>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Teams.Update(mapped);
                _unitOfWork.SaveChanges();
            }
            return status;
        }
    }
}
