﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.BLL.Services
{
    public class TaskStatesService : IService<TaskStateDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskStatesService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(TaskStateDTO item)
        {
            var mapped = _mapper.Map<TaskState>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            _unitOfWork.States.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var result = _unitOfWork.States.Delete(id);
            _unitOfWork.SaveChanges();
            return result;
        }

        public TaskStateDTO Get(int id)
        {
            var result = _unitOfWork.States.Get(id);

            return _mapper.Map<TaskStateDTO>(result);
        }

        public IEnumerable<TaskStateDTO> GetAll()
        {
            var result = _unitOfWork.States.GetAll();

            return _mapper.Map<IEnumerable<TaskStateDTO>>(result);
        }

        public bool Update(TaskStateDTO item)
        {
            var status = false;
            var mapped = _mapper.Map<TaskState>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.States.Update(mapped);
                _unitOfWork.SaveChanges();
            }
            return status;
        }
    }
}
