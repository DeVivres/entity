﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project.BLL.Services
{
    public class TasksService : IService<TaskDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(TaskDTO item)
        {
            var mapped = _mapper.Map<Task>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }
            _unitOfWork.Tasks.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var result = _unitOfWork.Tasks.Delete(id);
            _unitOfWork.SaveChanges();
            return result;
        }

        public TaskDTO Get(int id)
        {
            var result = _unitOfWork.Tasks.Get(id);
            return _mapper.Map<TaskDTO>(result);
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            var result = _unitOfWork.Tasks.GetAll();
            return _mapper.Map<IEnumerable<TaskDTO>>(result);
        }

        public bool Update(TaskDTO item)
        {
            var status = false;
            var mapped = _mapper.Map<Task>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Tasks.Update(mapped);
                _unitOfWork.SaveChanges();
            }
            return status;
        }
    }
}
