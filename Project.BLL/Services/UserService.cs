﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Project.BLL.Services
{
    public class UsersService : IService<UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public bool Create(UserDTO item)
        {
            var mapped = _mapper.Map<User>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            _unitOfWork.Users.Create(mapped);
            _unitOfWork.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var result = _unitOfWork.Users.Delete(id);
            _unitOfWork.SaveChanges();
            return result;
        }

        public UserDTO Get(int id)
        {
            var result = _unitOfWork.Users.Get(id);
            return _mapper.Map<UserDTO>(result);
        }

        public IEnumerable<UserDTO> GetAll()
        {
            var result = _unitOfWork.Users.GetAll();
            return _mapper.Map<IEnumerable<UserDTO>>(result);
        }

        public bool Update(UserDTO item)
        {
            var status = false;
            var mapped = _mapper.Map<User>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (Validator.TryValidateObject(mapped, context, results, true))
            {
                status = _unitOfWork.Users.Update(mapped);

                _unitOfWork.SaveChanges();
            }
            return status;
        }
    }
}
