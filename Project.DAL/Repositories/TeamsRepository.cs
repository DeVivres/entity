﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.DAL.Repositories
{
    public class TeamsRepository : IRepository<Team>
    {
        private readonly DatabaseContext _context;

        public TeamsRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void Create(Team item) => _context.Teams.Add(item);

        public bool Delete(int id)
        {
            var item = _context.Teams.FirstOrDefault(t => t.Id == id);

            if (item == null) return false;

            _context.Teams.Remove(item);
            return true;
        }

        public Team Get(int id) => _context.Teams.FirstOrDefault(t => t.Id == id);
        public IEnumerable<Team> GetAll() => _context.Teams;

        public bool Update(Team item)
        {
            var exists = _context.Teams.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
