﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.DAL.Repositories
{
    public class UsersRepository : IRepository<User>
    {
        private readonly DatabaseContext _context;

        public UsersRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void Create(User item) => _context.Users.Add(item);

        public bool Delete(int id)
        {
            var item = _context.Users.FirstOrDefault(u => u.Id == id);

            if (item == null) return false;

            _context.Users.Remove(item);
            return true;
        }

        public User Get(int id) => _context.Users.FirstOrDefault(u => u.Id == id);
        public IEnumerable<User> GetAll() => _context.Users;

        public bool Update(User item)
        {
            var exists = _context.Users.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
