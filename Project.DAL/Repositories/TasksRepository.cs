﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project.DAL.Repositories
{
    public class TasksRepository : IRepository<Task>
    {
        private readonly DatabaseContext _context;

        public TasksRepository(DatabaseContext context)
        {
            _context = context;
        }

        public void Create(Task item) => _context.Tasks.Add(item);

        public bool Delete(int id)
        {
            var item = _context.Tasks.FirstOrDefault(t => t.Id == id);

            if (item == null) return false;

            _context.Tasks.Remove(item);
            return true;
        }

        public Task Get(int id) => _context.Tasks.FirstOrDefault(t => t.Id == id);
        public IEnumerable<Task> GetAll() => _context.Tasks;

        public bool Update(Task item)
        {
            var exists = _context.Tasks.Contains(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
