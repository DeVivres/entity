﻿using Project.DAL.Entities;
using System.Collections.Generic;

namespace Project.ConsoleApplication.Services
{
    class DataCollection
    {
        private readonly HttpService _httpService;

        public ICollection<DAL.Entities.Project> Projects;
        public ICollection<Task> Tasks;
        public ICollection<Team> Teams;
        public ICollection<User> Users;

        public DataCollection()
        {
            _httpService = new HttpService();

            Projects = _httpService.GetAllProjects().Result;
            Tasks = _httpService.GetAllTasks().Result;
            Teams = _httpService.GetAllTeams().Result;
            Users = _httpService.GetAllUsers().Result;
        }
    }
}
