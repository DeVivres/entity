﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project.BLL.Interfaces;
using Project.Common.DTO;

namespace Project.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IService<TeamDTO> _teamsService;

        public TeamsController(IService<TeamDTO> teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _teamsService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _teamsService.Get(id);

            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TeamDTO team)
        {
            var result = _teamsService.Create(team);

            if (!result)
            {
                return BadRequest(team);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] TeamDTO team)
        {
            var result = _teamsService.Update(team);

            if (!result)
            {
                return BadRequest(team);
            }
            return Ok(team);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _teamsService.Delete(id);

            if (!result)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
