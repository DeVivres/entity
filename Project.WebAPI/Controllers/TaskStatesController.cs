﻿using Microsoft.AspNetCore.Mvc;
using Project.BLL.Interfaces;
using Project.Common.DTO;

namespace Project.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private readonly IService<TaskStateDTO> _statesService;

        public TaskStatesController(IService<TaskStateDTO> statesService)
        {
            _statesService = statesService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _statesService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _statesService.Get(id);

            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskStateDTO state)
        {
            var result = _statesService.Create(state);

            if (!result)
            {
                return BadRequest(state);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] TaskStateDTO state)
        {
            var result = _statesService.Update(state);

            if (!result)
            {
                return BadRequest(state);
            }
            return Ok(state);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _statesService.Delete(id);

            if (!result)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
